<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| EMAIL CONFING
| -------------------------------------------------------------------
| Configuration of outgoing mail server.
| */
$config['protocol']='smtp';
$config['smtp_host']='ssl://smtp.googlemail.com';
$config['smtp_port']=465;
$config['smtp_timeout']=30;
// Enter email
$config['smtp_user']='johnsymple@gmail.com';
// Enter password
$config['smtp_pass']='scientist';
$config['mailtype'] = 'html';
$config['charset']='utf-8';
$config['newline']="\r\n";

/* End of file email.php */
/* Location: ./system/application/config/email.php */  