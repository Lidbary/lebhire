<?php

/**
 * Created by IntelliJ IDEA.
 * User: John Muteti
 * Date: 6/4/2015
 * Time: 4:15 PM
 */
class Home extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('is_logged_in')) {
            $this->dashboard();
        } else {
            redirect('auth/restricted');
        }
    }

    public function dashboard()
    {
        if ($this->session->userdata('is_logged_in')) {
            $this->load->view('home_view');
        } else {
            redirect('auth/restricted');
        }
    }

}