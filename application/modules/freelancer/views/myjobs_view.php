<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu'); ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-home text-yellow"></i>
                    <span>Dashboard</span></a></li>
            <li><a href="<?php echo base_url(); ?>freelancer/myjobs"><i class="fa fa-folder-o text-yellow"></i>
                    <span>My Jobs</span></a></li>
            <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a>
            </li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-archive text-yellow"></i>
                    <span>Disputes</span></a></li>
            <li><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user text-yellow"></i>
                    <span>Profile</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Job
            <small>Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Jobs</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div>
            <h2>Running Jobs</h2>
            <hr style="border-radius: 10px; border-color: black; margin-top: -5px;">
        </div>


        <?php if (isset($view_data)) {
            foreach ($view_data as $key => $data): ?>
                <div class="box_myjobs">
                    <div style="margin-left:10px">
                        <h3 class="text-light-blue"><?php echo $data['title'] ?></h3>

                        <p class="job_desc comment"><?php echo $data['bid_details']; ?></p>

                        <p><strong>Amount:</strong> $<?php echo $data['bid_amount']; ?> &nbsp;&nbsp;&nbsp;
                            <strong>Finish in:</strong> <?php echo $data['completion_time']; ?>
                            <a href="<?php echo base_url(); ?>freelancer/submit_job/<?php echo $data['job_id'] ?>"
                               class="btn btn-flat bg-olive">Submit Job</a>
                        </p>
                    </div>
                </div>
            <?php endforeach;
        } else {
            echo "<p>Sorry, you do not have any current job. Keeps bidding.</p>";
        }?>

    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('partials/footer'); ?>
