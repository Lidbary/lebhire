<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu'); ?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-home text-yellow"></i>
                    <span>Dashboard</span></a></li>
            <li><a href="<?php echo base_url(); ?>freelancer/myjobs"><i class="fa fa-folder-o text-yellow"></i>
                    <span>My Jobs</span></a></li>
            <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a>
            </li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-archive text-yellow"></i>
                    <span>Disputes</span></a></li>
            <li><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user text-yellow"></i>
                    <span>Profile</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Job Feed</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div>
            <?php if (isset($kadispute) != '') { ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    You have a pending dispute. Please go to <a
                        href="<?php echo base_url(); ?>dispute"><strong>Disputes</strong></a>
                    to get this solved.
                </div>
            <?php } else {
            } ?>
        </div>

        <div>
            <h2>Job Listing</h2>
            <hr style="border-radius: 10px; border-color: black; margin-top: -5px;">
        </div>

        <?php foreach ($view_data as $key => $data): ?>
            <h3 class="text-light-blue"><?php echo $data['title'] ?></h3>
            <p class="job_desc comment"><?php echo $data['description'] ?></p>
            <p><strong>Client Budget:</strong> $<?php echo $data['amount'] ?> &nbsp;&nbsp;&nbsp;
                <strong>Time:</strong> <?php echo $data['time_estimate'] ?> &nbsp;&nbsp;&nbsp; <input type="button"
                                                                                                      class="btn btn-flat bg-yellow-active"
                                                                                                      value="BID"
                                                                                                      onclick="location.href='<?php echo base_url(); ?>freelancer/job_application/<?php echo $data['job_id'] ?>' ">
            </p>
        <?php endforeach; ?>
    </section>
    <!-- /.content -->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".comment").shorten({
            "showChars": 300,
            "moreText": "See More",
            "lessText": "Shorten"
        });
    });
</script>

<?php $this->load->view('partials/footer'); ?>

