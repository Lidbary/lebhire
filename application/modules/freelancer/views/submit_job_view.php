<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu'); ?>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-home text-yellow"></i>
                        <span>Dashboard</span></a></li>
                <li><a href="<?php echo base_url(); ?>freelancer/myjobs"><i class="fa fa-folder-o text-yellow"></i>
                        <span>My Jobs</span></a></li>
                <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i> <span>Messages</span></a>
                </li>
                <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-archive text-yellow"></i>
                        <span>Disputes</span></a></li>
                <li><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user text-yellow"></i>
                        <span>Profile</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Job
                <small>Submission</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Submit Job</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <h3>Job Title: <?php echo $view_data['title']; ?></h3>
            <?php echo form_open('freelancer/submit'); ?>

            <input type="hidden" name="job_id" value="<?php echo $view_data['job_id']; ?>">

            <div class="form-group">
                <label>Message</label>
                <textarea name="message" class="form-control" cols="4"></textarea>
            </div>

            <div class="form-group">
                <label>Job URL</label>
                <input type="text" name="job_url" class="form-control">
            </div>

            <div class="form-group">
                <?php echo form_submit('save', 'Submit', 'class="btn btn-success btn-large"'); ?>
            </div>

            <?php form_close(); ?>
        </section>
        <!-- /.content -->
    </div>
<?php $this->load->view('partials/footer'); ?>