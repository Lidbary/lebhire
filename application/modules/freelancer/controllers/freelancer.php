<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: John Muteti
 * Date: 6/4/2015
 * Time: 4:15 PM
 */
class Freelancer extends CI_Controller
{

    public function  __construct()
    {
        parent::__construct();
        $this->load->model('freelancer/freelancer_model', 'freelancer');
        $this->load->model('client/jobs_model', 'jobs');
    }

    public function index()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            $data['view_name'] = 'freelancer/freelancer_view';
            $data['view_data'] = $this->freelancer->get();
            $data['kadispute'] = $this->freelancer->check_disputes();
            $this->load->view('freelancer_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function job_application($id = null)
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            if ($id == null) {
                show_error('No identifier provided', 500);
            } else {
                $data['view_name'] = 'freelancer/job_application_view';
                $data['view_data'] = $this->freelancer->get($id);
                $this->load->view('job_application_view', $data);
            }
        } else {
            redirect('auth/restricted');
        }
    }

    public function bid()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            if (isset($_POST) && $_POST['save'] == 'Bid') {
                $data['job_id'] = $this->input->post('job_id');
                $data['freelancer_id'] = $this->input->post('freelancer_id');
                $data['bid_amount'] = $this->input->post('bid_amount');
                $data['bid_details'] = $this->input->post('bid_details');
                $data['completion_time'] = $this->input->post('completion_time');
                $this->freelancer->place_bid($data);
                redirect('freelancer');
            } else {
                redirect('freelancer/job_application');
            }
        } else {
            redirect('auth/restricted');
        }
    }

    public function myjobs()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            $data['view_name'] = 'freelancer/myjobs_view';
            $data['view_data'] = $this->freelancer->get();
            $data['view_data'] = $this->freelancer->get_myjobs();
            $this->load->view('myjobs_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function submit_job($id = null)
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            if ($id == null) {
                show_error('No identifier provided', 500);
            } else {
                $data['view_name'] = 'freelancer/submit_job_view';
                $data['view_data'] = $this->freelancer->get($id);
                $this->load->view('submit_job_view', $data);
            }
        } else {
            redirect('auth/restricted');
        }
    }

    public function submit()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            if (isset($_POST) && $_POST['save'] == 'Submit') {
                $data['job_id'] = $this->input->post('job_id');
                $data['message'] = $this->input->post('message');
                $data['job_url'] = $this->input->post('job_url');
                $this->freelancer->submit_job($data);
                redirect('freelancer');
            } else {
                redirect('freelancer/myjobs');
            }
        } else {
            redirect('auth/restricted');
        }
    }

}