<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: John Muteti
 * Date: 6/6/2015
 * Time: 4:42 PM
 */
class Freelancer_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        $this->db->select()->from('jobs');
        if ($id != null) {
            $this->db->where('job_id', $id);
        } else {
            $this->db->order_by('time_updated', 'desc');
        }

        $query = $this->db->query("select *, (bid_accepted) from jobs, bid WHERE bid_accepted='0' ORDER BY jobs.time_updated DESC ");
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function place_bid($data)
    {
        if (isset($data['job_application_id'])) {
            $this->db->where('job_application_id', $data['job_application_id']);
            $this->db->update('bid', $data);
        } else {
            $this->db->insert('bid', $data);
        }
    }

    public function get_myjobs($bid_id = null)
    {
        $this->db->select()->from('bid');
        if ($bid_id != null) {
            $this->db->where('job_application_id', $bid_id);
        } else {
            $this->db->order_by('job_application_id');
        }

        $cur_usr_id = $this->session->userdata('user_id');
        $query = $this->db->query("SELECT freelancer_id, bid_amount, bid_details, completion_time, title, jb.job_id FROM bid AS bd INNER JOIN jobs as jb ON bd.job_id = jb.job_id WHERE bid_accepted = 1 AND freelancer_id = '$cur_usr_id'");
        if ($bid_id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function check_disputes($dispute_id = null)
    {
        $this->db->select()->from('disputes');
        if ($dispute_id != null) {
            $this->db->where('$dispute_id', $dispute_id);
        } else {
            $this->db->order_by('$dispute_id');
        }

        $cur_usr_id = $this->session->userdata('user_id');
        $query = $this->db->query("SELECT message, initiated_by, user_id FROM disputes as ds INNER JOIN bid AS bd ON ds.job_id=bd.job_id INNER JOIN users as usr ON bd.freelancer_id = usr.user_id WHERE user_id = '$cur_usr_id'");
        if ($dispute_id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function submit_job($data)
    {
        if (isset($data['job_status_id'])) {
            $this->db->where('job_status_id', $data['job_status_id']);
            $this->db->update('job_status', $data);
        } else {
            $this->db->insert('job_status', $data);
        }
    }
}