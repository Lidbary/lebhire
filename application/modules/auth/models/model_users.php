<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Model_users extends CI_Model
{
    public function can_log_in()
    {
        $this->db->where('email', $this->input->post('email'));
        $this->db->where('username', $this->input->post('username'));
        $this->db->where('type', $this->input->post('type'));
        $this->db->where('password', md5($this->input->post('password')));

        $query = $this->db->get('users');

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function add_temp_users($key)
    {
        $data = array(
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'type' => $this->input->post('type'),
            'password' => md5($this->input->post('password')),
            'key' => $key
        );

        $query = $this->db->insert('temp_users', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function is_key_valid($key)
    {
        $this->db->where('key', $key);
        $query = $this->db->get('temp_users');

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }


    public function add_user($key)
    {
        $this->db->where('key', $key);
        $temp_user = $this->db->get('temp_users');

        if ($temp_user) {
            $row = $temp_user->row();

            $data = array(
                'email' => $row->email,
                'username' => $row->username,
                'type' => $row->type,
                'password' => $row->password
            );

            $did_add_user = $this->db->insert('users', $data);
        }

        if ($did_add_user) {
            $this->db->where('key', $key);
            $this->db->delete('temp_users');
            return $data['email'];
        }
        return false;
    }

    public function get_user($user_id = null)
    {
        if ($user_id != null) {
            $query = $this->db->query("select * from users where user_id = '$user_id'");
            return $query->result_array();
        }
    }

    public function get_usertype($email = null)
    {
        if ($email != null) {
            $query = $this->db->query("select * from users where email = '$email'");
            $result = $query->result_array();
            return $result[0]['type'];
        }
    }

    public function get_user_id($email = null)
    {
        if ($email != null) {
            $query = $this->db->query("select * from users where email = '$email'");
            $result = $query->result_array();
            return $result[0]['user_id'];
        }
    }

    public function get_username($email = null)
    {
        if ($email != null) {
            $query = $this->db->query("select * from users where email = '$email'");
            $result = $query->result_array();
            return $result[0]['username'];
        }
    }
}
