<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: John Muteti
 * Date: 6/4/2015
 * Time: 11:56 AM
 */
class Auth extends CI_Controller
{

    //leads to login
    public function index()
    {
        $this->login();
    }

    //login controller
    public function login()
    {
        $this->load->view('login_view');
    }

    //signup controller
    public function signup()
    {
        $this->load->view('signup_view');
    }

    //registered members view controller
    public function members()
    {
        if ($this->session->userdata('is_logged_in')) {
            $this->load->view('members/home');
        } else {
            redirect('auth/restricted');
        }

    }

    //restricted access view controller
    public function restricted()
    {
        $this->load->view('restricted');
    }

    //Validate login form for possible errors
    public function  login_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email',
            'required|trim|xss_clean|callback_validate_credentials');

        $this->form_validation->set_rules('password', 'Password', 'required|md5|trim');

        if ($this->form_validation->run()) {
            $user_type = $this->model_users->get_usertype($this->input->post('email'));
            $user_id = $this->model_users->get_user_id($this->input->post('email'));
            $username = $this->model_users->get_username($this->input->post('email'));
            $data = array(
                'email' => $this->input->post('email'),
                'is_logged_in' => 1,
                'user_type' => $user_type,
                'user_id' => $user_id,
                'username' => $username
            );

            $this->session->set_userdata($data);
            switch ($user_type) {
                case 'freelancer':
                    redirect('freelancer');
                    break;

                case 'client':
                    redirect('client');
                    break;

                case 'admin':
                    redirect('admin');
                    break;

                default:
                    redirect('home');
                    break;
            }
        } else {
            $this->load->view('login_view');
        }

    }

    //signup form validation
    public function signup_validation()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'Email',
            'required|trim|valid_email|is_unique[users.email]');

        $this->form_validation->set_rules('username', 'Username', 'required|trim');

        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        $this->form_validation->set_rules('cpassword', 'Confirm Password',
            'required|trim|matches[password]');

        $this->form_validation->set_rules('type', 'Type', 'required');

        $this->form_validation->set_message('is_unique', "This email has already been registered!");

        if ($this->form_validation->run()) {

            $key = md5(uniqid());

            //send email to user
            $this->load->library('email');
            $this->load->model('model_users');
            $this->email->from('johnsymple@gmail.com', 'Account');
            $this->email->to($this->input->post('email'));
            $this->email->subject("Confirm your account");

            $message = "<p>Thankyou for registering with us.</p>";
            $message .= "<p><a href='" . base_url() . "auth/register_user/$key' >Click here </a> to confirm your account and login in to your Lebhire account.</p>";

            $this->email->message($message);

            if ($this->email->send()) {
                $this->model_users->add_temp_users($key);
                echo "The confirmation email has been send to your account.";
            } else {
                echo "Sorry, we did not successfully verify you email.";
            }

        } else {
            $this->load->view('signup_view');
        }
    }

    //check if user input compare to data model data
    public function validate_credentials()
    {
        $this->load->model('model_users');

        if ($this->model_users->can_log_in()) {
            return true;
        } else {
            $this->form_validation->set_message('validate_credentials', 'Incorrect username/email or password.');
            return false;
        }
    }

    //logout controller
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth/login');
    }

    //confirm user registration and move him to data model
    public function register_user($key)
    {
        $this->load->model('model_users');

        if ($this->model_users->is_key_valid($key)) {
            if ($newemail = $this->model_users->add_user($key)) {
                $data = array(
                    'email' => $newemail,
                    'is_logged_in' => 0
                );
                $this->session->set_userdata($data);
                redirect('auth/login');
            } else echo "User registration failed. Try again later.";
        } else echo "Invalid Key";
    }

    public function restrict_users($user_types)
    {
        /*
        *   The usertypes parameter is an array variable of users that are
        *   not allowed access into the particular route.
        *   @example usage:
        *   restrict_users(array('freelancer', 'client', 'support'))
        */
        if ($user_types == null) {
            return false;
        } else {
            if ($this->session->userdata('is_logged_in')) {
                $user_type = $this->session->userdata('user_type');
                // check if the user should be restricted and redirect him.
                // Otherwise do nothing
                foreach ($user_types as $types) {
                    if ($user_type == $types) {
                        redirect('auth/restricted');
                    }
                }
            } else {
                // If the user is not logged in, redirect to log in
                redirect('auth/login');
            }
        }
    }

}

