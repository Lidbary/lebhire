<!DOCTYPE html>
<html lang="en">

<?php
// checking for the header array and the data inside
if (isset($header) && is_array($header)) {
    $this->load->view('partials/header', $header);
} else {
    $this->load->view('partials/header');
}
?>

<body class="login-page" style="overflow: hidden">
<div class="login-box">
    <div class="login-logo">
        <a href=""><b>Leb</b>Hire</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Log in to start</p>

        <form action="<?php echo base_url(); ?>auth/login_validation" method="post">
            <?php
            $message = validation_errors();
            if ($message != null) {
                echo '<div class="alert alert-danger alert-dismissable">';
                echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                echo validation_errors();
                echo '</div>';
            }
            ?>
            <div class="form-group has-feedback">
                <input type="email" name="email" class="form-control" placeholder="Email" required
                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password" required/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group ha-feedback">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="<?php echo base_url() . "auth/signup"; ?>" class="text-center">Register as new member</a>

    </div>
</div>

<?php $this->carabiner->display('js'); ?>
<?php $this->carabiner->display('css'); ?>

<!-- jQuery 2.1.3 -->
<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url(); ?>assets//bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>

</html>