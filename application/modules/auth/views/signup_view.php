<!DOCTYPE html>
<html lang="en">

<?php
// checking for the header array and the data inside
if (isset($header) && is_array($header)) {
    $this->load->view('partials/header', $header);
} else {
    $this->load->view('partials/header');
}
?>

<body class="register-page">
<div class="register-box">
    <div class="register-logo">
        <a href=""><b>Leb</b>Hire</a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg"><strong>Please sign up</strong></p>
        <?php echo form_open('auth/signup_validation'); ?>

        <div class="form-group">
            <label>Email:</label>
            <?php echo form_input('email', $this->input->post('email'), 'class="form-control"'); ?>
            <?php echo form_error('email'); ?>
        </div>
        <div class="form-group has-feedback">
            <label>Username:</label>
            <?php echo form_input('username', $this->input->post('username'), 'class="form-control"'); ?>
            <?php echo form_error('username'); ?>
        </div>
        <div class="form-group has-feedback">
            <label>Password</label>
            <?php echo form_password('password', $this->input->post('password'), 'class="form-control"', 'required'); ?>
            <?php echo form_error('password'); ?>
        </div>
        <div class="form-group has-feedback">
            <label>Password Confirmation</label>
            <?php echo form_password('cpassword', $this->input->post('cpassword'), 'class="form-control"'); ?>
            <?php echo form_error('cpassword'); ?>
        </div>
        <div class="form-group has-feedback">
            <label>I want to</label>
            <select name="type" class="form-control">
                <option value="freelancer">Work</option>
                <option value="client">Hire</option>
            </select>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="checkbox icheck">
                    <label>
                        <input type="checkbox" name="terms"
                               oninvalid="this.setCustomValidity('Please agree to our terms and conditions.')"> I agree
                        to Lebhire <a href="#">Terms &amp; Conditons</a>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <input type="submit" name="signup_submit" class="btn btn-primary btn-block btn-flat" value="Register"/>
            </div>
            <!-- /.col -->
            <div class="col-xs-8">
                I already a memeber?<a href="login" class="text-center"> <strong>Login</strong></a>
            </div>
        </div>

        <?php echo form_close(); ?>
    </div>
</div>

<?php $this->carabiner->display('js'); ?>
<?php $this->carabiner->display('css'); ?>

<script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

</body>
</html>