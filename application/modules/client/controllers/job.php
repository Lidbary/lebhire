<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: John Muteti
 * Date: 6/6/2015
 * Time: 12:03 PM
 */
class Job extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('is_logged_in')) {
            $this->load->view('create_job_view');
        } else {
            redirect('auth/restricted');
        }
    }

    public function job_details($job_id)
    {
        if ($this->session->userdata('is_logged_in')) {
            $this->load->model('jobs_model');
            $this->load->model('auth/model_users', 'users');
            $data['job'] = $this->jobs_model->get($job_id);
            $bids = $this->jobs_model->get_bids($job_id);
            $bids_updated = null;
            $i = 0;
            foreach ($bids as $bid) {
                $freelancer = $this->users->get_user($bid['freelancer_id']);
                $bids_updated[$i]['freelancer_name'] = $freelancer[0]['username'];
                $bids_updated[$i]['freelancer_id'] = $freelancer[0]['user_id'];
                $bids_updated[$i]['bid_amount'] = $bid['bid_amount'];
                $bids_updated[$i]['bid_details'] = $bid['bid_details'];
                $i++;
            }
            $data['bids'] = $bids_updated;
            $this->load->view('job_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }
}