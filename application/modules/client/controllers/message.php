<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: John Muteti
 * Date: 6/6/2015
 * Time: 11:56 AM
 */
class Message extends CI_Controller
{

    public function contact($user_id = null)
    {
        if ($this->session->userdata('is_logged_in') &&
            $this->session->userdata('user_type') == 'client' &&
            $user_id != null
        ) {

            $this->load->model('auth/model_users', 'users');
            $recepient = $this->users->get_user($user_id);
            $data['recepient_name'] = $recepient[0]['username'];
            $data['sender_id'] = $this->session->userdata('user_id');
            $data['recepient_id'] = $user_id;

            $this->load->view("compose_view", $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function send_message()
    {
        if ($this->session->userdata('is_logged_in')) {
            $this->load->model("message/message_model", "message_model");
            $user_id = $this->input->post('recepient_id');
            $prompt = $this->message_model->send_message();
            $data['msg'] = $prompt ? "Message sent." : "Sending failed.";
            $this->load->model('auth/model_users', 'users');
            $recepient = $this->users->get_user($user_id);
            $data['recepient_name'] = $recepient[0]['username'];
            $data['sender_id'] = $this->session->userdata('user_id');
            $data['recepient_id'] = $user_id;

            $this->load->view("compose_view", $data);
        } else {
            redirect('auth/restricted');
        }
    }

}