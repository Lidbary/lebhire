<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Client extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('is_logged_in')) {
            $this->listing();
        } else {
            redirect('auth/restricted');
        }
    }

}