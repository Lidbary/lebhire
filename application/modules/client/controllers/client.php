<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: John Muteti
 * Date: 6/5/2015
 * Time: 3:03 PM
 */
class Client extends CI_Controller
{

    public function  __construct()
    {
        parent::__construct();
        $this->load->model('client/jobs_model', 'jobs');
        $this->load->model('auth/model_users', 'users');
    }

    public function index()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            $data['view_name'] = 'client/client_view';
            $data['view_data'] = $this->jobs->get_my_jobs($this->session->userdata('user_id'));
            $this->load->view('client_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function create_job()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            $data['view_name'] = 'client/create_job_view';
            $data['view_data'] = '';
            $this->load->view('create_job_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function save()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            if (isset($_POST) && $_POST['save'] == 'Add') {
                $data['title'] = $this->input->post('title');
                $data['description'] = $this->input->post('description');
                $data['category'] = $this->input->post('category');
                $data['time_estimate'] = $this->input->post('time_estimate');
                $data['amount'] = $this->input->post('amount');
                $data['created_by'] = $this->input->post('created_by');
                $this->jobs->add($data);
                redirect('client');
            } else {
                redirect('client/create_job');
            }

        } else {
            redirect('auth/restricted');
        }
    }

    public function contracts()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            $data['view_name'] = 'client/contracts_view';
            $data['view_data'] = $this->jobs->get_current_contract();
            $this->load->view('contracts_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }
}