<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu_client'); ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo base_url(); ?>client"><i class="fa fa-home text-yellow"></i> <span>Dashboard</span></a>
            </li>
            <li class="active"><a href="<?php echo base_url(); ?>client/contracts"><i
                        class="fa fa-folder-o text-yellow"></i>
                    <span>Contracts</span></a></li>
            <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a></li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-bar-chart text-yellow"></i>
                    <span>Disputes</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Job
            <small>Contacts</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>client"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">My Contracts</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div>
            <h2>Current Contracts</h2>
            <hr style="border-radius: 10px; border-color: black; margin-top: -5px;">
        </div>

        <?php foreach ($view_data as $key => $data): ?>
            <h3 class="text-light-blue"><?php echo $data['title'] ?></h3>
            <p class="job_desc comment"><?php echo $data['description']; ?></p>
            <p><strong>My Budget:</strong> $<?php echo $data['amount']; ?> &nbsp;&nbsp;&nbsp;
                <strong>Time:</strong> <?php echo $data['time_estimate']; ?> &nbsp;&nbsp;&nbsp;
                <strong>Freelancer: </strong> <?php echo $data['username']; ?>
            </p>
        <?php endforeach; ?>

    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('partials/footer'); ?>
