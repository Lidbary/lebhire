<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu_client'); ?>

    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="active"><a href=""><i class="fa fa-home text-yellow"></i> <span>Dashboard</span></a></li>
                <li><a href="<?php echo base_url(); ?>client/contracts"><i class="fa fa-folder-o text-yellow"></i>
                        <span>Contracts</span></a>
                </li>
                <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i> <span>Messages</span></a>
                </li>
                <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-bar-chart text-yellow"></i> <span>Disputes</span></a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>
                    <button class="btn btn-flat bg-olive" onclick="location.href='client/create_job'">Create Job
                    </button>
                </small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="client"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>3</h3>

                            <p>Total Jobs</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-folder"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>2</h3>

                            <p>Current Jobs</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-clipboard"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>100<sup style="font-size: 20px">%</sup></h3>

                            <p>Hire Rate</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ribbon-a"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>$ 65</h3>

                            <p>Total Spent</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-cash"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div>
                <h2>Open Jobs</h2>
                <hr style="border-radius: 10px; border-color: black; margin-top: -5px;">
            </div>

            <?php if (isset($view_data)) {
                foreach ($view_data as $data): ?>
                    <h3 class="text-light-blue">
                        <a href="<?php echo base_url(); ?>client/job/job_details/<?php echo $data['job']['job_id']; ?>">
                            <?php echo $data['job']['title']; ?>
                        </a>
                    </h3>
                    <p><strong>My Budget:</strong> $<?php echo $data['job']['amount']; ?> &nbsp;&nbsp;&nbsp;
                        <strong>Time:</strong> <?php echo $data['job']['time_estimate']; ?> &nbsp;&nbsp;&nbsp;
                        <strong>Bids: <?php echo $data['bids']; ?></strong>
                    </p>
                <?php endforeach;
            } else {
                echo "<p>No open jobs.</p>";
            }?>
        </section>
        <!-- /.content -->
    </div>

<?php $this->load->view('partials/footer'); ?>