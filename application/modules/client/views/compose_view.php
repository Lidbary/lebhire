<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu_client'); ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo base_url(); ?>client"><i class="fa fa-home text-yellow"></i> <span>Dashboard</span></a>
            </li>
            <li><a href="<?php echo base_url(); ?>client/contracts"><i class="fa fa-folder-o text-yellow"></i>
                    <span>Contracts</span></a></li>
            <li class="active"><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a></li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-bar-chart text-yellow"></i>
                    <span>Disputes</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Compose Message&nbsp&nbsp
            <small>Recepient: <?php echo $recepient_name ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Job Details</li>
            <li class="active">Compose Message</li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">
        <div id="sent_message" class="bg-yellow">
            <?php
            if (isset($msg)) {
                echo $msg;
            }
            ?>
        </div>
        <div class="box box-primary">
            <form action="<?php echo base_url(); ?>client/message/send_message" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <input class="form-control" placeholder="To:" disabled="true"
                               value="<?php echo $recepient_name ?>"/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="Subject:" name="subject"/>
                    </div>
                    <div class="form-group">
                        <textarea id="compose-textarea" class="form-control" name="message"
                                  style="height: 300px"></textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                </div>
                <!-- /.box-footer -->
                <input name="recepient_id" value="<?php echo $recepient_id; ?>" hidden/>
                <input name="sender_id" value="<?php echo $sender_id; ?>" hidden/>
            </form>
        </div>

    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
    $(document).ready(
        function () {
            setTimeout(function () {
                $("#sent_message").fadeOut("slow");
            }, 3000);
        });

</script>

<?php $this->load->view('partials/footer'); ?>
