<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu_client'); ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo base_url(); ?>client"><i class="fa fa-home text-yellow"></i> <span>Dashboard</span></a>
            </li>
            <li class="active"><a href="<?php echo base_url(); ?>client/contracts"><i
                        class="fa fa-folder-o text-yellow"></i>
                    <span>Contracts</span></a></li>
            <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a></li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-bar-chart text-yellow"></i>
                    <span>Disputes</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $job['title']; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Job Details</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div>
            <h4>Details</h4>
            <hr style="border-radius: 10px; border-color: black; margin-top: -5px;">
        </div>

        <p><?php echo $job['description']; ?></p>
        <br><br>
        <?php if ($bids != null) { ?>
            <div class="box">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Freelancer</th>
                        <th>Reputation</th>
                        <th>Bid</th>
                        <th>Bid Details</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($bids as $bid) { ?>
                        <tr>
                            <td><?php echo $bid['freelancer_name']; ?></td>
                            <td><?php echo "(reviews/ratings)"; ?></td>
                            <td><?php echo $bid['bid_amount']; ?></td>
                            <td><?php echo $bid['bid_details']; ?></td>
                            <td><a class="fa fa-envelope-o"
                                   href="<?php echo base_url(); ?>client/message/contact/<?php echo $bid['freelancer_id']; ?>"></a>
                            </td>
                        </tr>
                    <?php }    //end for each ?>
                    </tbody>
                </table>
            </div>
        <?php } else {
            echo "<p class='alert alert-danger alert-dismissable'>
            	Sorry you do not have any bids for this job.</p>";
        }
        ?>
    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('partials/footer'); ?>
