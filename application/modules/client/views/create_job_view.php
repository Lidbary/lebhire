<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu_client'); ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active"><a href=""><i class="fa fa-home text-yellow"></i> <span>Dashboard</span></a></li>
            <li><a href="<?php echo base_url(); ?>client/contracts"><i class="fa fa-folder-o text-yellow"></i> <span>Contracts</span></a>
            </li>
            <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a></li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-bar-chart text-yellow"></i>
                    <span>Disputes</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Job
            <small>Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>client"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Create Job</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open('client/save'); ?>

        <input type="hidden" name="created_by" value="<?php
        echo $this->session->userdata('user_id');
        ?>">

        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control">
        </div>

        <div class="form-group">
            <label>Description</label>
            <textarea class="form-control" name="description"></textarea>
        </div>

        <div class="form-group">
            <label>Category</label>
            <select class="form-control" name="category">
                <option value="0">Select Category</option>
                <option value="Computer">Computer</option>
            </select>
        </div>

        <div class="form-group">
            <label>Time Estimate</label>
            <select name="time_estimate" class="form-control">
                <option value="0">Set time</option>
                <option value="1 day">1 day</option>
                <option value="1 week">1 week</option>
                <option value="2 weeks">2 weeks</option>
            </select>
        </div>

        <div class="form-group">
            <label>Amount</label>
            <input type="text" class="form-control" name="amount">
        </div>

        <div class="form-group">
            <?php echo form_submit('save', 'Add', 'class="btn btn-success" '); ?>
        </div>

        <?php echo form_close(); ?>
    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('partials/footer'); ?>
