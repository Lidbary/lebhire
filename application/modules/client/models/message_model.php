<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Message_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_client_messages($client_id = null)
    {
        if ($client_id != null) {
            $query = $this->db->query("select * from messages where client_id = '$client_id'");
            return $query->result_array();
        }
        return false;
    }

    public function get_freelancer_messages($freelancer_id = null)
    {
        if ($freelancer_id != null) {
            $query = $this->db->query("select * from messages where freelancer_id = '$freelancer_id'");
            return $query->result_array();
        }
        return false;
    }

    public function get_unread_messages($client_id = null)
    {
        if ($client_id = !null) {
            $query = $this->db->query("select * from messages where client_id = '$client_id' and message_read = '0'");
            return $query->result_array();
        }
        return false;
    }

    public function send_message()
    {
        $recepient_id = $this->input->post('recepient_id');
        $sender_id = $this->input->post('sender_id');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');

        return $this->db->query("insert into messages (sender_id,recepient_id,subject,message) 
            values ('$sender_id','$recepient_id','$subject','$message')");
    }
}