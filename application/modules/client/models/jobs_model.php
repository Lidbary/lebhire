<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: John Muteti
 * Date: 6/6/2015
 * Time: 12:19 PM
 */
class Jobs_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null)
    {
        $this->db->select()->from('jobs');
        if ($id != null) {
            $this->db->where('job_id', $id);
        } else {
            $this->db->order_by('job_id');
        }

        $query = $this->db->get();
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function get_my_jobs($user_id = null)
    {
        if ($user_id != null) {
            $query = $this->db->query("select *, (bid_accepted)  from jobs as jb INNER JOIN bid as bd ON bd.job_id=jb.job_id where bid_accepted = '0' AND created_by = '$user_id' order by jb.time_updated");
            $result = $query->result_array();
            $jobs = null;
            $i = 0;
            foreach ($result as $job) {
                $bids_number = $this->total_bids($job['job_id']);
                $jobs[$i]['job'] = $job;
                $jobs[$i]['bids'] = $bids_number;
                $i++;
            }
            return $jobs;
        }
        return null;
    }

    public function total_bids($job_id = null)
    {
        if ($job_id != null) {
            $query = $this->db->query("select count(job_id) as bids_number from bid where job_id = '$job_id'");
            $result = $query->result_array();
            return $result[0]['bids_number'];
        }
        return false;
    }

    public function add($data)
    {
        if (isset($data['job_id'])) {
            $this->db->where('job_id', $data['job_id']);
            $this->db->update('jobs', $data);
        } else {
            $this->db->insert('jobs', $data);
        }
    }

    public function get_bids($job_id = null)
    {
        if ($job_id != null) {
            $query = $this->db->query("select * from bid where job_id = '$job_id'");
            $result = $query->result_array();
            return $result;
        }
        return false;
    }

    public function get_current_contract($job_id = null)
    {
        $this->db->select()->from('jobs');
        if ($job_id != null) {
            $this->db->where('job_id', $job_id);
        } else {
            $this->db->order_by('job_id');
        }

        $cur_usr_id = $this->session->userdata('user_id');
        $query = $this->db->query("select * from jobs AS jb INNER JOIN bid AS bd ON jb.job_id = bd.job_id INNER JOIN users as usr ON bd.freelancer_id = usr.user_id WHERE created_by = '$cur_usr_id' AND bid_accepted=1");
        if ($job_id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
}