<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
*	The model for reviews. A review row has the folowing fields:
*	client_id(who wrote the review), freelancer_id, job_id(what job is being
*	reviewed), rating, review(actual message) and timestamp.
*
*/

class Reviews_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_client_reviews($client_id = null)
    {
        if ($client_id != null) {
            $query = $this->db->query("select * from reviews where client_id = '$client_id'");
            return $query->result_array();
        }
    }

    public function get_freelancer_reviews($freelancer_id = null)
    {
        if ($freelancer_id != null) {
            $query = $this->db->query("select * from reviews where freelancer_id = '$freelancer_id'");
            return $query->result_array();
        }
    }

    public function get_job_review($job_id = null)
    {
        if ($job_id != null) {
            $query = $this->db->query("select * from reviews where job_id = '$job_id'");
            $result = $query->result_array();

            if ($result == null) {
                return false;
            } else {
                return $result[0];
            }
        }
    }

    public function add_review()
    {

    }
}