<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Review extends CI_Controller
{


    public function  __construct()
    {
        parent::__construct();
        $this->load->model('reviews/reviews_model', 'jobs');
    }

    public function index()
    {
        if ($this->session->userdata('is_logged_in')) {

            $user_type = $this->session->userdata('user_type');
            $user_id = $this->session->userdata('user_id');
            $this->load->model('reviews_model');
            $reviews = null;
            switch ($user_type) {
                case 'freelancer':
                    $reviews = get_freelancer_reviews($user_id);
                    break;
                case 'client':
                    $reviews = get_client_reviews($user_id);
                    break;
            }

            $data['reviews'] = $reviews;
            $this->load->view('reviews/reviews_view', $data);

        }
    }


}