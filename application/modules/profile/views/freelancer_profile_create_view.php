<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu'); ?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-home text-yellow"></i>
                    <span>Dashboard</span></a></li>
            <li><a href="<?php echo base_url(); ?>freelancer/myjobs"><i class="fa fa-folder-o text-yellow"></i>
                    <span>My Jobs</span></a></li>
            <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a>
            </li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-archive text-yellow"></i>
                    <span>Disputes</span></a></li>
            <li class="active"><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user text-yellow"></i>
                    <span>Profile</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Profile
            <small>Manage</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Create Profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <script type="text/javascript"
                src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
        <script type="text/javascript">
            google.maps.event.addDomListener(window, 'load', function () {
                var places = new google.maps.places.Autocomplete(document.getElementById('location'));
                google.maps.event.addListener(places, 'place_changed', function () {
                    var place = places.getPlace();
                    var address = place.formatted_address;
                    var latitude = place.geometry.location.A;
                    var longitude = place.geometry.location.F;
                    var mesg = "Address: " + address;
                    mesg += "\nLatitude: " + latitude;
                    mesg += "\nLongitude: " + longitude;
//                    alert(mesg);
                });
            });
        </script>

        <div class="col-md-7">
            <?php echo form_open('profile/save'); ?>
            <div class="form-group">
                <label>Full Name</label>

                <div class="form-inline">
                    <input type="text" name="first_name" placeholder="First name" class="form-control" required>
                    <input type="text" name="last_name" placeholder="Last name" class="form-control" required>
                </div>
            </div>

            <div class="form-group">
                <label>Location</label>
                <input type="text" name="location" id="location" class="form-control" required>
            </div>

            <div class="form-group">
                <label>Skills</label>
                <textarea name="skills" class="form-control" cols="1"></textarea>
            </div>

            <div class="form-group">
                <label>Availability</label>
                <input type="text" name="availability" class="form-control" required>
            </div>

            <div class="form-group">
                <label>Short Description</label>
                <input type="text" name="show_desc" class="form-control" placeholder="Short description of you..."
                       required>
            </div>

            <div class="form-group">
                <label>Overview</label>
                <textarea name="main_desc" class="form-control" cols="4"
                          placeholder="Write a full description of yourself and what you do..."></textarea>
            </div>

            <input type="hidden" name="user_id" value="<?php
            echo $this->session->userdata('user_id');
            ?>">

            <div class="form-group">
                <?php echo form_submit('save', 'Create', 'class="btn btn-success" '); ?>
            </div>

            <?php echo form_close(); ?>
        </div>

    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('partials/footer'); ?>



