<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu'); ?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-home text-yellow"></i>
                    <span>Dashboard</span></a></li>
            <li><a href="<?php echo base_url(); ?>freelancer/myjobs"><i class="fa fa-folder-o text-yellow"></i>
                    <span>My Jobs</span></a></li>
            <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a>
            </li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-archive text-yellow"></i>
                    <span>Disputes</span></a></li>
            <li class="active"><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user text-yellow"></i>
                    <span>Profile</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Profile
            <small>My profile</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if (isset($view_data)) {
            foreach ($view_data as $data): ?>
                <div class="col-lg-8" style="margin-left: 20px;">
                    <h3>
                        <strong><?php echo $data['profyl']['first_name']; ?> <?php echo $data['profyl']['last_name']; ?></strong>
                    </h3>

                    <p><strong><?php echo $data['profyl']['show_desc']; ?></strong></p>

                    <div style="margin-left: 20px;">
                        <h5><i class="fa fa-map-marker"> <?php echo $data['profyl']['location']; ?></i></h5>

                        <p id="local_time"></p>
                    </div>

                    <h4><strong>Overview</strong></h4>
                    <hr style="border-radius: 10px; border-color: #4c4c4c; margin-top: -5px; margin-bottom: 5px;">
                    <p><?php echo $data['profyl']['main_desc']; ?></p>
                </div>
                <div class="col-lg-3" style="margin-top: 10px;">
                    <h4><strong>Work History</strong></h4>
                    <hr style="border-radius: 10px; border-color: #4c4c4c; margin-top: -5px; margin-bottom: 5px;">
                    <h5>No data</h5>

                    <h4><strong>Skills</strong></h4>
                    <hr style="border-radius: 10px; border-color: #4c4c4c; margin-top: -5px; margin-bottom: 5px;">
                    <?php
                    $skills = $data['profyl']['skills'];
                    $skills = explode(',', $skills);
                    foreach ($skills as $value) { ?>
                        <button class="btn btn-sm bg-orange btn-flat margin"><?php echo $value ?></button>
                    <?php } ?>

                    <h4><strong>Availability</strong></h4>
                    <hr style="border-radius: 10px; border-color: #4c4c4c; margin-top: -5px; margin-bottom: 5px;">
                    <h5><?php echo $data['profyl']['availability']; ?></h5>
                </div>
            <?php endforeach;
        } else { ?>
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                <strong>No profile has been created.</strong>
                <button onclick="location.href='profile/create'" class="btn btn-flat bg-olive">Create Profile
                </button>
            </div>
        <?php } ?>

    </section>
    <!-- /.content -->
</div>
<script type="text/javascript">
    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();

    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    var suffix = "AM";
    if (hours >= 12) {
        suffix = "PM";
        hours = hours - 12;
    }
    if (hours == 0) {
        hours = 12;
    }
    var full_time = hours + ":" + minutes + " " + suffix + " local time";
    document.getElementById("local_time").innerHTML = full_time;

</script>

<?php $this->load->view('partials/footer'); ?>



