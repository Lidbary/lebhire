<?php

/**
 * Created by IntelliJ IDEA.
 * User: rogue0x
 * Date: 6/30/15
 * Time: 11:45 AM
 */
class Profile_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function my_profile($user_id = null)
    {
        if ($user_id != null) {
            $query = $this->db->query("SELECT * FROM profile WHERE user_id = '$user_id' ");
            $result = $query->result_array();
            $prof = null;
            $i = 0;
            foreach ($result as $profyl) {
                $prof[$i]['profyl'] = $profyl;
                $i++;
            }
            return $prof;
        }
    }

    public function create($data)
    {
        if (isset($data['profile_id'])) {
            $this->db->where('profile_id', $data['profile_id']);
            $this->db->update('profile', $data);
        } else {
            $this->db->insert('profile', $data);
        }
    }
}