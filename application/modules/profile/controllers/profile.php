<?php

/**
 * Created by IntelliJ IDEA.
 * User: rogue0x
 * Date: 6/30/15
 * Time: 11:08 AM
 */
class Profile extends CI_Controller
{
    public function  __construct()
    {
        parent::__construct();
        $this->load->model('profile/profile_model', 'kaprofile');
    }

    public function index()
    {
        if ($this->session->userdata('is_logged_in')) {
            $this->myProfile();
        } else {
            redirect('auth/restricted');
        }
    }

    public function myProfile()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            $data['view_name'] = 'profile/freelancer_profile_view';
            $data['view_data'] = $this->kaprofile->my_profile($this->session->userdata('user_id'));
            $this->load->view('freelancer_profile_view', $data);
        } elseif ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            $this->load->view('client_profile_view');
        } else {
            redirect('auth/restricted');
        }
    }

    public function create()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            $this->load->view('freelancer_profile_create_view');
        } elseif ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            $this->load->view('client_profile_create_view');
        } else {
            redirect('auth/restricted');
        }
    }

    public function save()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            if (isset($_POST) && $_POST['save'] == 'Create') {
                $data['user_id'] = $this->input->post('user_id');
                $data['first_name'] = $this->input->post('first_name');
                $data['last_name'] = $this->input->post('last_name');
                $data['show_desc'] = $this->input->post('show_desc');
                $data['location'] = $this->input->post('location');
                $data['skills'] = $this->input->post('skills');
                $data['main_desc'] = $this->input->post('main_desc');
                $data['availability'] = $this->input->post('availability');
                $this->kaprofile->create($data);
                redirect('profile', 'refresh');
            } else {
                redirect('profile/create', 'refresh');
            }
        } elseif ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {

        }

    }
}