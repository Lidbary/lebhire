<?php
$this->load->view('partials/header');
$user_type = $this->session->userdata('user_type');
if ($user_type == 'freelancer') {
    $this->load->view('partials/menu');
    $this->load->view('partials/freelancer_sidebar');
}
else if($user_type == 'client') {
  $this->load->view('partials/menu_client');
  $this->load->view('partials/client_sidebar');
}
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Messages &nbsp&nbsp
          <small>conversation thread with <?php echo $contact['username']; ?></small>
      </h1>
  </section>
	<section class="content" style="padding: 30px">
		<div id="sent_message" class="bg-yellow">
    		<?php
    			if(isset($msg))
    			{
    				echo $msg;
    			}
    		?>	
    </div>
		<style type="text/css" media="screen">
			.single-message {
				background-color: #f5f5f5;
				padding: 15px;
				border: 1px solid #eeeeee;
			}	
		</style>
		<div class="row">
			<?php
			$numItems = count($messages);
			$i = 0;
				foreach ($messages as $message) { 
			?>
			<div class="single-message">
				<h4<?php
				if(++$i === $numItems) {
    			echo " id=\"$message_id\"";
  			}	?>
				><?php echo $message['username']; ?></h4>
				<h4><?php echo $message['subject']; ?> &nbsp&nbsp
					<small><?php echo $message['timestamp']; ?></small></h4>
				<p><?php echo $message['message']; ?></p>
			</div>
			<?php
				}
			?>
		</div>
		<div class="row">
			<form action="<?php echo base_url(); ?>message/message/send_message" method="post" accept-charset="utf-8">
	    <div class="box box-primary">
	      <div class="box-header with-border">
	        <h3 class="box-title">Compose Reply</h3>
	      </div><!-- /.box-header -->
	      <div class="box-body">
	        <div class="form-group">
	          <input class="form-control" placeholder="To:" value="<?php echo $contact['username']; ?>" disabled/>
	        </div>
	        <div class="form-group">
	          <input class="form-control" placeholder="Subject:" name="subject"/>
	        </div>
	        <div class="form-group">
	          <textarea id="compose-textarea" class="form-control" style="height: 100px" name="message"></textarea>
	        </div>
	      </div>
	      <div class="box-footer">
	      	<button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
	      </div>
	    </div>
	      <input	name="recepient_id" value="<?php echo $contact['user_id']; ?>" hidden/>
	      <input	name="sender_id" value="<?php echo $this->session->userdata('user_id'); ?>" hidden/>
	    </form>
    </div>
	</section>
</div>

<script type="text/javascript">
	$(document).ready(
		function(){
			setTimeout(function(){
				$("#sent_message").fadeOut("slow");
			}, 3000);
		});
	
</script>

<?php $this->load->view('partials/footer'); ?>	