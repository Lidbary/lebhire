<?php
$this->load->view('partials/header');
$user_type = $this->session->userdata('user_type');
if ($user_type == 'freelancer') {
    $this->load->view('partials/menu');
    $this->load->view('partials/freelancer_sidebar');
}
else if($user_type == 'client') {
  $this->load->view('partials/menu_client');
  $this->load->view('partials/client_sidebar');
}
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Messages&nbsp&nbsp
            <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Messages</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div id="sent_message" class="bg-yellow">
        <?php
          if(isset($msg))
          {
            echo $msg;
          }
        ?>  
      </div>
        <?php if ($messages != false) { ?>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">All Messages</h3>

                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                            <input type="text" class="form-control input-sm" placeholder="Search Mail"/>
                            <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="mailbox-controls">
                        <!-- Check all button -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                            <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                        </div>
                        <!-- /.btn-group -->
                        <div class="pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                <button class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                            </div>
                            <!-- /.btn-group -->
                        </div>
                        <!-- /.pull-right -->
                    </div>
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover table-striped">
                            <tbody>
                            <?php foreach ($messages as $message) { ?>
                                <tr>
                                    <td><input type="checkbox"/></td>
                                    <td class="mailbox-name">
                                        <a href="<?php echo base_url(); ?>message/message/view_message/<?php echo $message['message_id'] ."#".$message['message_id']; ?>">
                                            <?php echo $message['username']; ?></a></td>
                                    <td class="mailbox-subject"><b><?php echo $message['subject']; ?></b></td>
                                    <td class="mailbox-subject"><?php echo $message['message']; ?></td>
                                    <td class="mailbox-date"><?php echo $message['time_elapsed']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <!-- /.table -->
                    </div>
                    <!-- /.mail-box-messages -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer no-padding">
                    <div class="mailbox-controls">
                        <!-- Check all button -->
                        <div class="btn-group">
                            <button class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                            <button class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                        </div>
                        <!-- /.btn-group -->
                        <div class="pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                <button class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                            </div>
                            <!-- /.btn-group -->
                        </div>
                        <!-- /.pull-right -->
                    </div>
                </div>
            </div>
        <?php } else {
            echo "No messages yet.";
        } ?>

    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('partials/footer'); ?>	