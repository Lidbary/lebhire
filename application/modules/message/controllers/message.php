<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message extends CI_Controller
{
	public function index($msg = null)
	{
		if ($this->session->userdata('is_logged_in')) {
			$this->listing($msg);
		} else {
			redirect('auth/restricted');
		}
	}

	public function listing($msg = null)
	{
		if ($this->session->userdata('is_logged_in')) {
			$this->load->model('message_model');
			$user_type = $this->session->userdata('user_type');
			$user_id = $this->session->userdata('user_id');
			$messages = $this->message_model->get_listing($user_id);
			$data['messages'] = $messages;
			if($msg != null) {
				$data['msg'] = $msg;
			}
			$this->load->view('message_view', $data);            
		}
		else {   
			redirect('auth/restricted');
		}
	}

	public function view_message($message_id = null, $msg = null)
	{
		if($message_id != null && $this->session->userdata('is_logged_in')) 
		{
			$this->load->model('message_model');
			$data['messages'] = $this->message_model->get_thread($message_id);
			$data['contact'] = $this->message_model->get_contact($message_id);
			$data['message_id'] = $message_id;
			if($msg != null) {
				$data['msg'] = $msg;
			}
			$this->load->view('thread_view', $data);
		}
		else {
			redirect('auth/restricted');
		}
	}

	public function send_message()
	{
		$this->load->model('message_model');
		$feedback = $this->message_model->send_message();
		$feedback = $feedback . "#". $feedback;
		$msg = "Reply sent successfully";
		$this->view_message($feedback, $msg);
	}
}