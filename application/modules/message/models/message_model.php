<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Message_model extends CI_Model
{

	public function __construct()	
	{
		parent::__construct();
	}

	public function get($user_id = null)
	{
		if($user_id != null)
		{
			$messages = $this->get_messages($user_id);
			$this->load->model('auth/model_users','users');
			if($messages!=null){
				$messages_updated = null;
				$i = 0;
				foreach ($messages as $message) {
					$contact_id = $message['sender_id'] == $user_id ? $message['recepient_id'] : $message['sender_id'];
					$contact = $this->users->get_user($contact_id);
					$messages_updated[$i]['user_id'] = $contact_id;
					$messages_updated[$i]['message_id'] = $message['message_id'];
					$messages_updated[$i]['username'] = $contact[0]['username'];
					$messages_updated[$i]['subject'] = $message['subject'];
					$messages_updated[$i]['time_elapsed'] = $this->get_elapsed_time($message['timestamp']);
					$i++;
				}
				return $messages_updated;            
			}
		}
		return false;
	}

	public function get_listing($user_id = null) {
		if($user_id != null)
		{
			$messages = $this->get_received_messages($user_id);
			$this->load->model('auth/model_users','users');
			if($messages!=null){
				$messages_updated = null;
				$i = 0;
				$unique_contacts = array();
				foreach ($messages as $message) {
					$contact_id = $message['sender_id'];
					if(array_search($contact_id, $unique_contacts) !== false) {
						continue;
					}
					$contact = $this->users->get_user($contact_id);
					array_push($unique_contacts, $contact_id);
					$messages_updated[$i]['user_id'] = $contact_id;
					$messages_updated[$i]['message_id'] = $message['message_id'];
					$messages_updated[$i]['username'] = $contact[0]['username'];
					$messages_updated[$i]['message'] = strlen($message['message']) >= 40 
												? substr($message['message'], 0, 40) . "..." : $message['message'];
					$messages_updated[$i]['subject'] = $message['subject'];
					$messages_updated[$i]['time_elapsed'] = $this->get_elapsed_time($message['timestamp']);
					$i++;
				}
				return $messages_updated;
			}
		}
	}

	public function get_sent_messages($sender_id = null)
	{
		if ($sender_id != null) {
			$query = $this->db->query("select * from messages where sender_id = '$sender_id' order by timestamp desc");
			return $query->result_array();
		}
		return false;
	}

	public function get_received_messages($recepient_id = null)
	{
		if ($recepient_id != null) {
			$query = $this->db->query("select * from messages where recepient_id = '$recepient_id' order by timestamp desc");
			return $query->result_array();
		}
		return false;
	}

	public function get_messages($user_id = null)
	{
		if($user_id != null)
		{
			$query = $this->db->query("select * from messages where recepient_id = '$user_id' or sender_id = '$user_id'
					order by timestamp");
			return $query->result_array();
		}
		return false;
	}

	public function get_unread_messages($user_id = null)
	{
		if ($user_id = !null) {
			$messages = $this->get_messages($user_id);
			$unreads = null;
			if(!$messages)
			{
				$unreads = false;
			}
			else
			{
				$i = 0;
				foreach ($messages as $message) {
					if($message['message_read'] == 0)
					{
						$unreads[$i] = $message;
						$i++;
					}
				}
			}
			return $unreads;
		}
		return false;
	}

	public function send_message()
	{
		$recepient_id = $this->input->post('recepient_id');
		$sender_id = $this->input->post('sender_id');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');

		$this->db->query("insert into messages (sender_id,recepient_id,subject,message) 
			values ('$sender_id','$recepient_id','$subject','$message')");
		return $this->db->insert_id();
	}

	public function get_message($message_id = null)
	{
		if($message_id != null)
		{
			$query = $this->db->query("select * from messages where message_id = '$message_id'");
			return $query->result_array();
		}
		return false;
	}

	public function get_thread($message_id = null) {
		if($message_id != null)
		{
			$message = $this->get_message($message_id);
			$user_id = $this->session->userdata('user_id');
			$contact_id = $user_id == $message[0]['sender_id'] ? $message[0]['recepient_id'] :
						$message[0]['sender_id'] ;
			$threads = null;
			$query = "
					select * from messages 
					where sender_id = '$user_id' and recepient_id = '$contact_id'
					or sender_id = '$contact_id' and recepient_id = '$user_id'
					order by timestamp";
			$messages = $this->db->query($query);
			$messages = $messages->result_array();
			$messages_updated = null;			
			$this->load->model('auth/model_users','users');
			$user = $this->users->get_user($user_id);
			$contact = $this->users->get_user($contact_id);
			$i = 0;
			foreach ($messages as $message) {
				$messages_updated[$i]['username'] = $message['sender_id'] == $user_id ?
						$user[0]['username'] : $contact[0]['username'];
				$messages_updated[$i]['timestamp'] = $message['timestamp'];
				$messages_updated[$i]['subject'] = $message['subject'];
				$messages_updated[$i]['message'] = $message['message'];
				$i++;
			}
			return $messages_updated;
		}
		return false;
	}

	public function get_contact($message_id = null) {
		if($message_id != null) {
			$message = $this->get_message($message_id);
			$user_id = $this->session->userdata('user_id');
			$contact_id = $user_id == $message[0]['sender_id'] ? $message[0]['recepient_id'] :
						$message[0]['sender_id'] ;		
			$this->load->model('auth/model_users','users');
			$contact = $this->users->get_user($contact_id);
			return $contact[0];			
		}
	}
	public function get_elapsed_time($timestamp, $full = false)
	{
		/*  Get the current time and subtract it by the timestamp provided, 
		*   to return string values such as...two seconds ago, 1 day ago e.t.c
		*/
		$now = new DateTime;
    $ago = new DateTime($timestamp);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
}