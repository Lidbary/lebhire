<?php

/**
 * Created by IntelliJ IDEA.
 * User: rogue0x
 * Date: 7/2/15
 * Time: 8:42 AM
 */
class Support_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function send($data)
    {
        if (isset($data['sr_id'])) {
            $this->db->where('sr_id', $data['sr_id']);
            $this->db->update('support_reports', $data);
        } else {
            $this->db->insert('support_reports', $data);
        }
    }
}