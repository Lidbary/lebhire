<?php

/**
 * Created by IntelliJ IDEA.
 * User: rogue0x
 * Date: 7/2/15
 * Time: 8:34 AM
 */
class Support extends CI_Controller
{
    public function  __construct()
    {
        parent::__construct();
        $this->load->model('support/support_model', 'support_md');
    }

    public function index()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            $data['view_name'] = 'support/freelancer_support_view';
            $this->load->view('freelancer_support_view', $data);
        } elseif ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            $data['view_name'] = 'support/client_support_view';
            $data['view_data'] = $this->support_md->get();
            $this->load->view('client_support_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function send()
    {
        $cuser_type = $this->session->userdata('user_type');
        $messages = array();
        $messages[] = "Thank you. We're looking onto this immediately.";
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            if (isset($_POST) && $_POST['send'] == 'Send') {
                $data['created_by'] = $this->input->post('created_by');
                $data['topic'] = $this->input->post('topic');
                $data['type'] = $this->input->post('type');
                $data['message'] = $this->input->post('message');
                $this->support_md->send($data);
                redirect('support');
            } else {
                redirect('support');
            }
        } elseif ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {

        } else {
            redirect('auth/restricted');
        }
    }
}