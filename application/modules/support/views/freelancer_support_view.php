<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu_support'); ?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Support
            <small> Welcome, <strong><?php echo $this->session->userdata('username'); ?></strong></small>
        </h1>
    </section>

    <section class="content">

        <div class="col-lg-8">
            <h3 style="text-align: center;">TELL US SOMETHING</h3>

            <form action="<?php echo base_url(); ?>support/send" method="post">
                <input type="hidden" name="created_by" value="<?php echo $this->session->userdata('user_id'); ?>">

                <div class="form-group">
                    <label>Topic</label>
                    <input type="text" name="topic" class="form-control" required>
                </div>

                <div class="form-group">
                    <label>Type</label>
                    <select name="type" class="form-control" required>
                        <option value="">Select type</option>
                        <option value="Help"> Help</option>
                        <option value="Bug"> Bug</option>
                        <option value="Suggestion"> Suggestion</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Message</label>
                    <textarea name="message" class="form-control" required></textarea>
                </div>

                <div class="form-group">
                    <input type="submit" name="send" value="Send" class="btn btn-primary">
                </div>
            </form>
        </div>

    </section>
</div>

<?php $this->load->view('partials/footer'); ?>

