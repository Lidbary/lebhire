<?php

/**
 * Created by IntelliJ IDEA.
 * User: rogue0x
 * Date: 7/2/15
 * Time: 5:36 PM
 */
class Admin_model extends CI_Model
{
    public function  __construct()
    {
        parent::__construct();
    }

    public function get_support($id = null) {
        $this->db->select()->from('support_reports');
        if ($id != null) {
            $this->db->where('sr_id', $id);
        } else {
            $this->db->order_by('date_created', 'desc');
        }

        $query = $this->db->query("SELECT sr_id,topic,srs.type,message,created_by,date_created,username from support_reports as srs INNER JOIN users as usr ON srs.created_by=usr.user_id WHERE is_read='0'");
        if ($id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
}