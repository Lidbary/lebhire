<?php

/**
 * Created by IntelliJ IDEA.
 * User: rogue0x
 * Date: 7/2/15
 * Time: 5:34 PM
 */
class Admin extends CI_Controller
{
    public function  __construct()
    {
        parent::__construct();
        $this->load->model('admin/admin_model', 'admin_md');
    }

    public function index()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'admin') {
            $data['view_name'] = 'admin/admin_view';
            $this->load->view('admin_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function support() {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'admin') {
            $data['view_name'] = 'admin/support_view';
            $data['view_data'] = $this->admin_md->get_support();
            $this->load->view('support_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }
}