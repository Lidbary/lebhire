<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu_admin'); ?>

<div class="content-wrapper">
    <section class="content">
        <div style="margin-top: 50px; margin-left: 50px;">
            <h3>Messages</h3>

            <div id="supot">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Support Messages</h3>
                    </div>
                    <div class="box-body">
                        <table id="mesg_table" class="table table-responsive table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="100px;">From</th>
                                <th>Topic</th>
                                <th>Message</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($view_data as $key => $data): ?>
                                <tr>
                                    <td><?php echo $data['username'] ?></td>
                                    <td><?php echo $data['topic'] ?></td>
                                    <td><?php echo $data['message'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(function () {
        $('#mesg_table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<?php $this->load->view('partials/footer'); ?>
