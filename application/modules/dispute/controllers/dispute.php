<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: rogue0x
 * Date: 6/12/15
 * Time: 8:38 AM
 */
class Dispute extends CI_Controller
{

    public function  __construct()
    {
        parent::__construct();
        $this->load->model('dispute/dispute_model', 'dispute');
    }

    public function index()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            $data['view_name'] = 'dispute/client_dispute_view';
            $data['view_data'] = $this->dispute->get_disputes();
            $this->load->view('client_dispute_view', $data);
        } elseif ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            $data['view_name'] = 'dispute/freelancer_dispute_view';
            $data['view_data'] = $this->dispute->get_freelancer_disputes();
            $this->load->view('freelancer_dispute_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function launch()
    {
        $cuser_type = $this->session->userdata('user_type');
        if ($this->session->userdata('is_logged_in') && $cuser_type == 'client') {
            $data['view_name'] = 'dispute/launch_dispute_view';
            $data['contracts'] = $this->dispute->get_contracts();
            $this->load->view('launch_dispute_view', $data);
        } elseif ($this->session->userdata('is_logged_in') && $cuser_type == 'freelancer') {
            $data['view_name'] = 'dispute/launch_dispute_freelancer_view';
            $data['contracts'] = $this->dispute->get_freelancer_contracts();
            $this->load->view('launch_dispute_freelancer_view', $data);
        } else {
            redirect('auth/restricted');
        }
    }

    public function create()
    {
        if ($this->session->userdata('is_logged_in')) {
            if (isset($_POST) && $_POST['save'] == 'Create') {
                $data['job_id'] = $this->input->post('job_id');
                $data['topic'] = $this->input->post('topic');
                $data['message'] = $this->input->post('message');
                $data['level'] = $this->input->post('level');
                $data['time_created'] = $this->input->post('time_created');
                $data['initiated_by'] = $this->input->post('initiated_by');
                $this->dispute->create($data);
                redirect('dispute');
            } else {
                redirect('dispute/launch');
            }

        } else {
            redirect('auth/restricted');
        }
    }
}