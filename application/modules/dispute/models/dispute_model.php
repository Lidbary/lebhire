<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: rogue0x
 * Date: 6/12/15
 * Time: 9:08 AM
 */
class Dispute_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_contracts()
    {
        $cur_usr_id = $this->session->userdata('user_id');
        $query = $this->db->query("select * from jobs AS jb INNER JOIN bid ON jb.job_id = bid.job_id WHERE created_by = '$cur_usr_id' AND bid_accepted=1");
        return $query->result();
    }

    public function get_freelancer_contracts()
    {
        $cur_usr_id = $this->session->userdata('user_id');
        $query = $this->db->query("SELECT freelancer_id, bid_amount, bid_details, completion_time, jb.job_id, title FROM bid AS bd INNER JOIN jobs as jb ON bd.job_id = jb.job_id WHERE bid_accepted = 1 AND freelancer_id = '$cur_usr_id'");
        return $query->result();
    }

    public function create($data)
    {
        if (isset($data['dispute_id'])) {
            $this->db->where('dispute_id', $data['dispute_id']);
            $this->db->update('disputes', $data);
        } else {
            $this->db->insert('disputes', $data);
        }
    }

    public function get_disputes($dispute_id = null)
    {
        $this->db->select()->from('disputes');
        if ($dispute_id != null) {
            $this->db->where('$dispute_id', $dispute_id);
        } else {
            $this->db->order_by('$dispute_id');
        }

        $cur_usr_id = $this->session->userdata('user_id');
        $query = $this->db->query("select topic, title, message, initiated_by, user_id, username FROM disputes AS ds INNER JOIN jobs as js ON ds.job_id=js.job_id INNER JOIN users AS usr ON ds.initiated_by=usr.user_id WHERE js.created_by='$cur_usr_id'");
        if ($dispute_id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function get_freelancer_disputes($dispute_id = null)
    {
        $this->db->select()->from('disputes');
        if ($dispute_id != null) {
            $this->db->where('$dispute_id', $dispute_id);
        } else {
            $this->db->order_by('$dispute_id');
        }

        $cur_usr_id = $this->session->userdata('user_id');
        $query = $this->db->query("SELECT topic, message, initiated_by, bd.job_id, user_id, title FROM disputes AS dis INNER JOIN bid AS bd ON dis.job_id = bd.job_id INNER JOIN users AS usr ON bd.freelancer_id = usr.user_id INNER JOIN jobs as jb ON dis.job_id = jb.job_id WHERE bid_accepted = '1' AND user_id = '$cur_usr_id'");
        if ($dispute_id != null) {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }
}