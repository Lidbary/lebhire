<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu'); ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-home text-yellow"></i>
                    <span>Dashboard</span></a></li>
            <li><a href="<?php echo base_url(); ?>freelancer/myjobs"><i class="fa fa-folder-o text-yellow"></i>
                    <span>My Jobs</span></a></li>
            <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i>
                    <span>Messages</span></a>
            </li>
            <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-archive text-yellow"></i>
                    <span>Disputes</span></a></li>
            <li><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user text-yellow"></i>
                    <span>Profile</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Contract
            <small>Disputes</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Launch Dispute</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open('dispute/create'); ?>

        <div class="form-group">
            <label>Contract</label>
            <select name="job_id" class="form-control">
                <option value="0">Select contract</option>
                <?php foreach ($contracts as $row) {
                    echo '<option value="' . $row->job_id . '">' . $row->title . '</option>';
                } ?>
            </select>
        </div>

        <div class="form-group">
            <label>Topic</label>
            <input type="text" class="form-control" name="topic"/>
        </div>

        <div class="form-group">
            <label>Message</label>
            <textarea class="form-control" name="message"></textarea>
        </div>

        <div class="form-group">
            <label>Level</label>
            <select class="form-control" name="level">
                <option value="0">Select Category</option>
                <option value="High">High</option>
                <option value="Medium">Medium</option>
                <option value="Low">Low</option>
            </select>
        </div>

        <?php $date = date('Y-m-d H:i:s'); ?>
        <input type="hidden" name="time_created" value="<?php print_r($date) ?>">

        <input type="hidden" name="initiated_by" value="<?php
        echo $this->session->userdata('user_id');
        ?>">

        <div class="form-group">
            <?php echo form_submit('save', 'Create', 'class="btn btn-success" '); ?>
        </div>

        <?php echo form_close(); ?>
    </section>
    <!-- /.content -->
</div>

<?php $this->load->view('partials/footer'); ?>
