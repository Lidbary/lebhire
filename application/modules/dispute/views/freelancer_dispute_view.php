<?php $this->load->view('partials/header'); ?>
<?php $this->load->view('partials/menu'); ?>

    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-home text-yellow"></i>
                        <span>Dashboard</span></a></li>
                <li><a href="<?php echo base_url(); ?>freelancer/myjobs"><i class="fa fa-folder-o text-yellow"></i>
                        <span>My Jobs</span></a></li>
                <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i> <span>Messages</span></a>
                </li>
                <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-archive text-yellow"></i>
                        <span>Disputes</span></a></li>
                <li><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user text-yellow"></i>
                        <span>Profile</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>
                    <button class="btn btn-flat bg-red"
                            onclick="location.href='<?php echo base_url(); ?>dispute/launch'">Launch Dispute
                    </button>
                </small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Disputes</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div>
                <h2>Contract Disputes</h2>
                <hr style="border-radius: 10px; border-color: black; margin-top: -5px;">
            </div>


            <?php if (isset($view_data)) {
                foreach ($view_data as $data): ?>
                    <div class="box_dispute">
                        <div style="margin-left:10px">
                            <h4 class="text-light-black" style="text-align: left">
                                <strong><?php echo $data['title'] ?></strong></h4>

                            <p><strong class="text-red">Topic: </strong> <?php echo $data['topic'] ?></p>

                            <p><?php echo $data['message'] ?></p>
                            <?php
                            $usr_id = $this->session->userdata('user_id');
                            $usr_mdl = $data['initiated_by'];
                            ?>
                            <p><strong class="text-red">Initiated By: </strong> <?php if ($usr_mdl === $usr_id) {
                                    echo 'Me';
                                } else {
                                    echo 'Client';
                                }
                                ?> </p>
                        </div>
                    </div>
                <?php endforeach;
            } else {
                echo "<p>You do not have any disputes.</p>";
            } ?>


        </section>
        <!-- /.content -->
    </div>

<?php $this->load->view('partials/footer'); ?>