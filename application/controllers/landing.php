<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends CI_Controller
{

    public function index()
    {
        $this->load->helper('form'); // loading the form helper on this page.
        $data['header']['page_title'] = 'Home'; // title for the page
        $data['content']['view_data'] = array(); // data coming inside the view

        $this->load->view('splash', $data);
    }

}
