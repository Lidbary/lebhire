<body class="sidebar-mini skin-blue">
<div class="wrapper">
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>freelancer" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>l</b>Hr</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Leb</b>Hire</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu" style="margin-right: 10px;">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="user-image"
                             alt="User Image"/>
            <span class="hidden-xs"><?php
                echo $this->session->userdata('username');
                ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?php echo $this->session->userdata('username'); ?> - Web Developer
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo base_url(); ?>profile" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-left" style="margin-left: 20px;">
                                <a href="<?php echo base_url(); ?>support" class="btn btn-default btn-flat">Support</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url(); ?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
