<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li><a href="<?php echo base_url(); ?>freelancer"><i class="fa fa-home text-yellow"></i>
                <span>Dashboard</span></a></li>
        <li><a href="<?php echo base_url(); ?>freelancer/myjobs"><i class="fa fa-folder-o text-yellow"></i>
                <span>My Jobs</span></a></li>
        <li><a href="<?php echo base_url(); ?>message"><i class="fa fa-envelope-o text-yellow"></i> <span>Messages</span></a>
        </li>
        <li><a href="<?php echo base_url(); ?>dispute"><i class="fa fa-archive text-yellow"></i>
                <span>Disputes</span></a></li>
        <li><a href="<?php echo base_url(); ?>profile"><i class="fa fa-user text-yellow"></i>
                <span>Profile</span></a></li>
    </ul>
</section>
<!-- /.sidebar -->
</aside>