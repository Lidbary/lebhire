<?php $this->load->view('partials/header'); ?>
<body>
    <section class="content">
        <div class="error-page">
            <h3 class="headline text-red" style="margin-top: -20px;">401</h3>
            <div class="error-content">
                <h3><i class="fa fa-warning text-red"></i> <strong class="text-capitalize text-red">Oops! You cannot be here</strong></h3>
                <p>
                    We not what you're trying. Just go <a href="javascript:history.back()" class="text-red">back </a>to where you came from <a href="<?php echo base_url(); ?>auth/login" class="text-red">login</a> again.
                </p>
            </div>
        </div>
    </section>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
</body>
