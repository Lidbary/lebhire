<!DOCTYPE html>
<html lang="en" style="overflow: hidden">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>OnlinepaidJob</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/splash/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/splash/css/agency.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/splash/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
</head>

<body id="page-top" class="index">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top">OnlinepaidJob</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li>
                    <a class="page-scroll" href="#portfolio">Cash Transfer</a>
                </li>
                <li>
                    <a class="page-scroll" href="#about">Portfolio</a>
                </li>
                <li>
                    <a class="page-scroll" href="#team">About Us</a>
                </li>
                <li>
                    <a class="page-scroll" href="auth">Sign In</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Header -->
<header>
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Welcome to OnlinepaidJob</div>
            <div class="intro-heading">It's Nice To Meet You</div>
            <a href="auth/signup" class="page-scroll btn btn-xl">Sign Up</a>
        </div>
    </div>
</header>


<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/splash/js/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/splash/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/splash/js/classie.js"></script>
<script src="<?php echo base_url(); ?>assets/splash/js/cbpAnimatedHeader.js"></script>
<script src="<?php echo base_url(); ?>assets/splash/js/jqBootstrapValidation.js"></script>
<script src="<?php echo base_url(); ?>assets/splash/js/contact_me.js"></script>
<script src="<?php echo base_url(); ?>assets/splash/js/agency.js"></script>

</body>

</html>
